package com.poccofinance.lms.loanApplication;

import com.poccofinance.lms.Application;
import com.poccofinance.lms.entity.LoanProduct;
import com.poccofinance.lms.entity.ProductTerms;
import com.poccofinance.lms.entity.StatusValue;
import com.poccofinance.lms.rest.loanApplication.CollectLoanApplicationRequestDTO;
import com.poccofinance.lms.rest.loanApplication.LoanApplicationController;
import com.poccofinance.lms.rest.loanApplication.LoanApplicationSummaryDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class LoanApplicationCollectedAndLoanGrantedTest {

    @LocalServerPort
    private int port;

    @Bean
    LoanProduct loanProduct(){
        ProductTerms productTerms = new ProductTerms(BigDecimal.ONE, BigDecimal.valueOf(10_000), 1, 30, LocalTime.MIN, LocalTime.MAX, 30);
        return new LoanProduct(BigDecimal.TEN, productTerms);
    }

    TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void testLoanApplicationCollectedAndLoanGranted() {

        CollectLoanApplicationRequestDTO request = givenProperRequest();

        HttpEntity<CollectLoanApplicationRequestDTO> entity = new HttpEntity<>(request, new HttpHeaders());

        ResponseEntity<Link> responseWithlinkToCollectedLoanApplication = restTemplate.exchange(
                createURLWithPort(LoanApplicationController.URI_REL),
                HttpMethod.PUT, entity, Link.class);

        assertTrue(responseWithlinkToCollectedLoanApplication.getStatusCode().is2xxSuccessful());

        ResponseEntity<LoanApplicationSummaryDTO> loanApplicationSummary = restTemplate.getForEntity(responseWithlinkToCollectedLoanApplication.getBody().getHref(), LoanApplicationSummaryDTO.class);
        assertTrue(loanApplicationSummary.getStatusCode().is2xxSuccessful());
        assertEquals(StatusValue.ACCEPTED, loanApplicationSummary.getBody().getStatus().getValue());
        assertTrue(loanApplicationSummary.getBody().getLoan().isPresent());
    }

    private CollectLoanApplicationRequestDTO givenProperRequest() {
        CollectLoanApplicationRequestDTO request = new CollectLoanApplicationRequestDTO();
        request.setCustomerEmail("email@domain.country");
        request.setRequestedAmount(BigDecimal.TEN);
        request.setRequestedRepayDays(20);
        return request;
    }

    private String createURLWithPort(String uri) {
        return new StringBuilder("http://localhost:")
                .append(port)
                .append(uri)
                .toString();
    }

}