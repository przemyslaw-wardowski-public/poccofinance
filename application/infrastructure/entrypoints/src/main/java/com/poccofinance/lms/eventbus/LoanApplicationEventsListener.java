package com.poccofinance.lms.eventbus;

import com.google.common.eventbus.Subscribe;
import com.poccofinance.lms.entity.events.LoanApplicationAcceptedEvent;
import com.poccofinance.lms.entity.events.LoanApplicationCreatedEvent;
import com.poccofinance.lms.entity.events.LoanApplicationIntegrityPassedEvent;
import com.poccofinance.lms.entity.events.LoanApplicationRejectedEvent;
import com.poccofinance.lms.usecase.loan.grant.GrantLoanUseCase;
import com.poccofinance.lms.usecase.loanApplication.integrityVerification.VerifyLoanApplicationIntegrityUseCase;
import com.poccofinance.lms.usecase.loanApplication.riskCalculation.CalculateLoanRiskUseCase;

public class LoanApplicationEventsListener {

    VerifyLoanApplicationIntegrityUseCase verifyLoanApplicationIntegrityUseCase;
    CalculateLoanRiskUseCase calculateLoanRiskUseCase;
    GrantLoanUseCase grantLoanUseCase;

    public LoanApplicationEventsListener(VerifyLoanApplicationIntegrityUseCase verifyLoanApplicationIntegrityUseCase, CalculateLoanRiskUseCase calculateLoanRiskUseCase, GrantLoanUseCase grantLoanUseCase){
        this.verifyLoanApplicationIntegrityUseCase = verifyLoanApplicationIntegrityUseCase;
        this.calculateLoanRiskUseCase = calculateLoanRiskUseCase;
        this.grantLoanUseCase = grantLoanUseCase;
    }

    @Subscribe
    public void on(LoanApplicationCreatedEvent event){
        verifyLoanApplicationIntegrityUseCase.verifyIntegrityOf(event.getAggregateId());
    }

    @Subscribe
    public void on(LoanApplicationIntegrityPassedEvent event){
        calculateLoanRiskUseCase.calculateRiskFor(event.getAggregateId());
    }

    @Subscribe
    public void on(LoanApplicationRejectedEvent event) {
        System.out.println("Application rejected");
    }

    @Subscribe
    public void on(LoanApplicationAcceptedEvent event) {
        grantLoanUseCase.grantLoanFor(event.getAggregateId());
    }

}
