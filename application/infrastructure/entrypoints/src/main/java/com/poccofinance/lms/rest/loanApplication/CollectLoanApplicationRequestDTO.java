package com.poccofinance.lms.rest.loanApplication;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CollectLoanApplicationRequestDTO {

    String customerEmail;
    BigDecimal requestedAmount;
    Integer requestedRepayDays;

}
