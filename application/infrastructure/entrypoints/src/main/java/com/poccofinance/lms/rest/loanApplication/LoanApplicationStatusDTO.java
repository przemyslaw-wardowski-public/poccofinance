package com.poccofinance.lms.rest.loanApplication;

import com.poccofinance.lms.entity.StatusValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class LoanApplicationStatusDTO {

    StatusValue value;
    String description;

}
