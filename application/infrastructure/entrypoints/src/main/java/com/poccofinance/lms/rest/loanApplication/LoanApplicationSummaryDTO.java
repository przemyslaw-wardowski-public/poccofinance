package com.poccofinance.lms.rest.loanApplication;

import com.poccofinance.lms.entity.LoanApplicationStatus;
import com.poccofinance.lms.entity.StatusValue;
import com.poccofinance.lms.rest.loan.LoanController;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.ResourceSupport;

import java.util.Optional;
import java.util.UUID;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class LoanApplicationSummaryDTO extends ResourceSupport {

    LoanApplicationStatusDTO status;
    Optional<LoanDTO> loan;

    public static LoanApplicationSummaryDTO from(UUID loanApplicationId, LoanApplicationStatus status) {
        LoanApplicationStatusDTO statusDTO = new LoanApplicationStatusDTO(status.getValue(), status.getDetails());
        LoanDTO loanDTO = new LoanDTO(loanApplicationId, linkTo(LoanController.class).slash(loanApplicationId).withSelfRel());
        Optional<LoanDTO> optionalLoan = status.getValue().equals(StatusValue.ACCEPTED) ? Optional.of(loanDTO) : Optional.empty();
        return new LoanApplicationSummaryDTO(statusDTO, optionalLoan);
    }
}
