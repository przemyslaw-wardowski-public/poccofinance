package com.poccofinance.lms.rest.loan;

import com.poccofinance.lms.entity.Loan;
import com.poccofinance.lms.usecase.loan.getOverview.GetOverviewUseCase;
import com.poccofinance.lms.usecase.loan.postponeRepayDate.PostponeRepayDateUseCase;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(LoanController.URI_REL)
public class LoanController {

    static final String URI_REL = "/api/loan";

    PostponeRepayDateUseCase postponeRepayDateUseCase;
    GetOverviewUseCase getOverviewUseCase;

    public LoanController(PostponeRepayDateUseCase postponeRepayDateUseCase, GetOverviewUseCase getOverviewUseCase) {
        this.postponeRepayDateUseCase = postponeRepayDateUseCase;
        this.getOverviewUseCase = getOverviewUseCase;
    }

    @GetMapping("/{loanID}")
    public ResponseEntity<LoanDTO> get(@PathVariable UUID loanID){
        Loan loan = getOverviewUseCase.getDetailsOf(loanID);
        return ResponseEntity.ok(LoanDTO.from(loan));
    }

    @GetMapping("/{loanID}/postpone")
    public ResponseEntity<?> postponeRepayDate(@PathVariable UUID loanID) {
        postponeRepayDateUseCase.postponeRepayDate(loanID);
        Loan loan = getOverviewUseCase.getDetailsOf(loanID);
        return ResponseEntity.ok(LoanDTO.from(loan));
    }

}
