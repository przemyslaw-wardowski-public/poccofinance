package com.poccofinance.lms.rest.loanApplication;

import com.poccofinance.lms.entity.LoanApplicationStatus;
import com.poccofinance.lms.entity.RequestedLoanTerms;
import com.poccofinance.lms.usecase.loanApplication.collection.CollectLoanApplicationUseCase;
import com.poccofinance.lms.usecase.loanApplication.getOverview.GetLoanApplicationStatusUseCase;
import org.springframework.hateoas.Link;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
@RequestMapping(LoanApplicationController.URI_REL)
public class LoanApplicationController {

    public static final String URI_REL = "/api/loanApplication";

    private final CollectLoanApplicationUseCase collectLoanApplicationUseCase;
    private final GetLoanApplicationStatusUseCase getLoanApplicationStatusUseCase;

    public LoanApplicationController(CollectLoanApplicationUseCase collectLoanApplicationUseCase, GetLoanApplicationStatusUseCase getLoanApplicationStatusUseCase) {
        this.collectLoanApplicationUseCase = collectLoanApplicationUseCase;
        this.getLoanApplicationStatusUseCase = getLoanApplicationStatusUseCase;
    }

    @GetMapping("/{loanApplicationId}")
    public LoanApplicationSummaryDTO get(@PathVariable UUID loanApplicationId) {
        LoanApplicationStatus status = getLoanApplicationStatusUseCase.getStatus(loanApplicationId);
        return LoanApplicationSummaryDTO.from(loanApplicationId, status);
    }

    @PutMapping
    public Link collect(@RequestBody CollectLoanApplicationRequestDTO requestDTO) {
        UUID applicationId = UUID.randomUUID();
        RequestedLoanTerms terms = new RequestedLoanTerms(requestDTO.getRequestedAmount(), requestDTO.getRequestedRepayDays());
        collectLoanApplicationUseCase.collect(applicationId, requestDTO.getCustomerEmail(), terms);
        return linkTo(LoanApplicationController.class)
                .slash(applicationId)
                .withSelfRel();
    }

}
