package com.poccofinance.lms.rest.loan;

import com.poccofinance.lms.entity.Loan;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.ResourceSupport;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class LoanDTO extends ResourceSupport {

    UUID loanId;
    LocalDateTime repayDate;
    BigDecimal originalTotalAmounToPay;

    public static LoanDTO from(Loan loan) {
        UUID loanId = loan.getLoanID();
        LocalDateTime repayDate = loan.getRepayDate();
        BigDecimal originalTotalAmounToPay = loan.getTotalAmounToPay();
        return new LoanDTO(loanId, repayDate, originalTotalAmounToPay);
    }
}
