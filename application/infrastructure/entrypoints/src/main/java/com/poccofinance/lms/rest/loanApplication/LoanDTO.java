package com.poccofinance.lms.rest.loanApplication;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class LoanDTO extends ResourceSupport {

    UUID loanId;

    public LoanDTO (UUID loanId, Link link) {
        this.loanId = loanId;
        add(link);
    }

}
