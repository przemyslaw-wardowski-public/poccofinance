package com.poccofinance.lms

import com.google.common.eventbus.EventBus
import com.poccofinance.lms.entity.Loan
import com.poccofinance.lms.entity.events.DomainEvent
import com.poccofinance.lms.usecase.loan.postponeRepayDate.LoanRepository

class InMemoryLoanRepositories implements LoanRepository, com.poccofinance.lms.usecase.loan.grant.LoanRepository, com.poccofinance.lms.usecase.loan.getOverview.LoanRepository {

    private EventBus eventBus
    private HashMap<UUID, List<DomainEvent>> loanEventStore = new HashMap<>()

    InMemoryLoanRepositories(EventBus eventBus) {
        this.eventBus = eventBus
    }

    @Override
    void save(Loan loan) {
        def currentEvents = loanEventStore.getOrDefault(loan.loanID, new ArrayList<>())
        currentEvents.addAll(loan.getChanges())
        loanEventStore.put(loan.getLoanID(), currentEvents)
        loan.changes.forEach { eventBus.post it }
        loan.flushChanges()
    }


    @Override
    Loan findById(UUID loanId) {
        List<DomainEvent> events = loanEventStore.get(loanId)
        Loan result = events.inject( new Loan() ) { Loan loan, DomainEvent event -> loan.apply event }
        result.flushChanges()
        return result
    }

}
