package com.poccofinance.lms

import com.google.common.eventbus.EventBus
import com.poccofinance.lms.entity.LoanApplication
import com.poccofinance.lms.entity.events.DomainEvent
import com.poccofinance.lms.usecase.loanApplication.collection.LoanApplicationCollectingRepository
import com.poccofinance.lms.usecase.loanApplication.integrityVerification.LoanApplicationRepository

class InMemoryLoanApplicationRepositories implements LoanApplicationRepository, LoanApplicationCollectingRepository, com.poccofinance.lms.usecase.loanApplication.getOverview.LoanApplicationRepository, com.poccofinance.lms.usecase.loan.grant.LoanApplicationRepository {

    private HashMap<UUID, List<DomainEvent>> loanApplicationEventStore = new HashMap<>()
    private EventBus eventBus

    InMemoryLoanApplicationRepositories(EventBus eventBus){
        this.eventBus = eventBus
    }

    @Override
    void save(LoanApplication loanApplication) {
        List<DomainEvent> currentEvents = loanApplicationEventStore.getOrDefault(loanApplication.getApplicationId(), new ArrayList<>())
        currentEvents.addAll(loanApplication.getChanges())
        loanApplicationEventStore.put(loanApplication.getApplicationId(), currentEvents)
        loanApplication.getChanges().forEach { eventBus.post it }
        loanApplication.flushChanges()
    }

    @Override
    LoanApplication findById(UUID loanApplicationId) {
        List<DomainEvent> events = loanApplicationEventStore.get(loanApplicationId)
        LoanApplication result = events.inject( new LoanApplication() ) { LoanApplication loanApplication, DomainEvent event -> loanApplication.apply event }
        result.flushChanges()
        return result
    }

}