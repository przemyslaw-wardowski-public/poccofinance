package com.poccofinance.lms.infrastructure;

import com.google.common.eventbus.EventBus;
import com.poccofinance.lms.InMemoryLoanApplicationRepositories;
import com.poccofinance.lms.InMemoryLoanRepositories;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataProvidersConfiguration {


    @Bean
    InMemoryLoanApplicationRepositories loanApplicationRepository(EventBus eventBus){
        return new InMemoryLoanApplicationRepositories(eventBus);
    }

    @Bean
    InMemoryLoanRepositories loanRepositories(EventBus eventBus) {
        return new InMemoryLoanRepositories(eventBus);
    }


}
