package com.poccofinance.lms.domain;

import com.poccofinance.lms.InMemoryLoanApplicationRepositories;
import com.poccofinance.lms.entity.LoanProduct;
import com.poccofinance.lms.entity.ProductTerms;
import com.poccofinance.lms.usecase.loanApplication.collection.CollectLoanApplicationUseCase;
import com.poccofinance.lms.usecase.loanApplication.collection.ProductPortfolioLookupRepository;
import com.poccofinance.lms.usecase.loanApplication.getOverview.GetLoanApplicationStatusUseCase;
import com.poccofinance.lms.usecase.loanApplication.integrityVerification.VerifyLoanApplicationIntegrityUseCase;
import com.poccofinance.lms.usecase.loanApplication.riskCalculation.CalculateLoanRiskUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.math.BigDecimal;
import java.time.LocalTime;

@Configuration
public class LoanApplicationUseCasesConfiguration {


    @Profile("!default")
    @Bean
    LoanProduct loanProduct(){
        ProductTerms productTerms = new ProductTerms(BigDecimal.ONE, BigDecimal.valueOf(10_000), 1, 30, LocalTime.of(6, 0), LocalTime.MIDNIGHT, 30);
        return new LoanProduct(BigDecimal.TEN, productTerms);
    }

    @Bean
    CollectLoanApplicationUseCase collectLoanApplicationUseCase(InMemoryLoanApplicationRepositories repo, LoanProduct loanProduct) {
        ProductPortfolioLookupRepository productPortfolioLookupRepository = () -> loanProduct;
        return new CollectLoanApplicationUseCase(productPortfolioLookupRepository, repo);
    }

    @Bean
    VerifyLoanApplicationIntegrityUseCase verifyLoanApplicationIntegrityUseCase(InMemoryLoanApplicationRepositories repo) {
        return new VerifyLoanApplicationIntegrityUseCase(repo);
    }

    @Bean
    CalculateLoanRiskUseCase calculateLoanRiskUseCase(InMemoryLoanApplicationRepositories repo){
        return new CalculateLoanRiskUseCase(repo);
    }

    @Bean
    GetLoanApplicationStatusUseCase getLoanApplicationStatusUseCase(InMemoryLoanApplicationRepositories repo) {
        return new GetLoanApplicationStatusUseCase(repo);
    }

}
