package com.poccofinance.lms.infrastructure;

import com.google.common.eventbus.EventBus;
import com.poccofinance.lms.eventbus.LoanApplicationEventsListener;
import com.poccofinance.lms.usecase.loan.grant.GrantLoanUseCase;
import com.poccofinance.lms.usecase.loanApplication.integrityVerification.VerifyLoanApplicationIntegrityUseCase;
import com.poccofinance.lms.usecase.loanApplication.riskCalculation.CalculateLoanRiskUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EntryPointsConfiguration {

    @Bean
    LoanApplicationEventsListener loanApplicationEventsListener(VerifyLoanApplicationIntegrityUseCase verifyLoanApplicationIntegrityUseCase, CalculateLoanRiskUseCase calculateLoanRiskUseCase, GrantLoanUseCase grantLoanUseCase, EventBus eventBus) {
        LoanApplicationEventsListener listener = new LoanApplicationEventsListener(verifyLoanApplicationIntegrityUseCase, calculateLoanRiskUseCase, grantLoanUseCase);
        eventBus.register(listener);
        return listener;
    }

}
