package com.poccofinance.lms.domain;

import com.poccofinance.lms.InMemoryLoanApplicationRepositories;
import com.poccofinance.lms.InMemoryLoanRepositories;
import com.poccofinance.lms.usecase.loan.getOverview.GetOverviewUseCase;
import com.poccofinance.lms.usecase.loan.grant.GrantLoanUseCase;
import com.poccofinance.lms.usecase.loan.postponeRepayDate.PostponeRepayDateUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoanUseCasesConfiguration {

    @Bean
    GrantLoanUseCase grantLoanUseCase(InMemoryLoanApplicationRepositories loanApplicationRepositories, InMemoryLoanRepositories loanRepositories){
        return new GrantLoanUseCase(loanApplicationRepositories, loanRepositories);
    }

    @Bean
    PostponeRepayDateUseCase postponeRepayDateUseCase(InMemoryLoanRepositories loanRepositories) {
        return new PostponeRepayDateUseCase(loanRepositories);
    }

    @Bean
    GetOverviewUseCase getOverviewUseCase(InMemoryLoanRepositories loanRepositories){
        return new GetOverviewUseCase(loanRepositories);
    }

}
