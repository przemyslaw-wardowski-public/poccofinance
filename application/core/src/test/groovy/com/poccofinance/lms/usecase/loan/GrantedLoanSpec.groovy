package com.poccofinance.lms.usecase.loan

import com.poccofinance.lms.entity.Loan
import com.poccofinance.lms.usecase.TestDataFactory
import com.poccofinance.lms.usecase.loan.grant.GrantLoanUseCase
import com.poccofinance.lms.usecase.loan.grant.LoanApplicationRepository
import com.poccofinance.lms.usecase.loan.grant.LoanRepository
import spock.lang.Specification

class GrantedLoanSpec extends Specification {


    LoanApplicationRepository loanApplicationRepository = Mock(LoanApplicationRepository)
    LoanRepository loanRepository = Mock(LoanRepository)

    GrantLoanUseCase grantLoanUseCase = new GrantLoanUseCase(loanApplicationRepository, loanRepository)

    def "Granted loan has interest calculated" () {

        given: "Product lookup that will find product"
            def loanApplication = TestDataFactory.LoanApplicationFactory.valid()
            loanApplicationRepository.findById(loanApplication.applicationId) >> loanApplication

        when: "Loan is granted"
            grantLoanUseCase.grantLoanFor loanApplication.applicationId

        then: "Loan contains interests calculated"
            1 * loanRepository.save({
                Loan loan -> loan.getInterest() == BigDecimal.ONE
            } as Loan)
    }

}
