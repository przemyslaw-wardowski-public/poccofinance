package com.poccofinance.lms.usecase.loanApplication.integrityVerification

import com.poccofinance.lms.entity.LoanApplication
import com.poccofinance.lms.entity.ProductTerms
import com.poccofinance.lms.entity.StatusValue
import com.poccofinance.lms.usecase.TestDataFactory
import spock.lang.Specification

import java.time.LocalTime

class VerifyRequestedAmountSpec extends Specification{

    LoanApplicationRepository repository = Mock(LoanApplicationRepository)
    VerifyLoanApplicationIntegrityUseCase reviewLoanAplicationUseCase = new VerifyLoanApplicationIntegrityUseCase(repository)

    def "Loan should be rejected when requested amount is out of product terms range" () {

        given: "Loan application for product with product"
            def terms = new ProductTerms(productMinAmount, productMaxAmount, 0, 30, LocalTime.MIN, LocalTime.MAX, 30)
            def product = TestDataFactory.ProductTestFactory.validWithTerms terms
            LoanApplication loanApplication = TestDataFactory.LoanApplicationFactory.validWithProductAndRequestedAmountLike (product, amount)
            repository.findById(loanApplication.getApplicationId()) >> loanApplication

        when: "Application for loan is processed "
            reviewLoanAplicationUseCase.verifyIntegrityOf loanApplication.getApplicationId()

        then: "Application should be rejected"
            loanApplication.status.value == StatusValue.REJECTED

        where:
            productMinAmount            | productMaxAmount      | amount
            BigDecimal.ONE              | BigDecimal.TEN        | productMinAmount - BigDecimal.ONE
            BigDecimal.ONE              | BigDecimal.TEN        | productMaxAmount + BigDecimal.ONE
    }


}
