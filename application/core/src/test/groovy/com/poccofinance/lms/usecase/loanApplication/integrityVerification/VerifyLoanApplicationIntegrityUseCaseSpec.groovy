package com.poccofinance.lms.usecase.loanApplication.integrityVerification

import com.poccofinance.lms.entity.StatusValue
import spock.lang.Specification

class VerifyLoanApplicationIntegrityUseCaseSpec extends Specification{

    LoanApplicationRepository repository = Mock(LoanApplicationRepository)
    VerifyLoanApplicationIntegrityUseCase verifyLoanAplicationIntegrityUseCase = new VerifyLoanApplicationIntegrityUseCase(repository)

    def "Loan application should pass integrity validation when all data provided and product terms are fulfilled" () {

        given: "Loan application with all data provided"
            def loanApplication = com.poccofinance.lms.usecase.TestDataFactory.LoanApplicationFactory.valid()
            repository.findById(loanApplication.getApplicationId()) >> loanApplication

        when: "Application processing is executed"
            verifyLoanAplicationIntegrityUseCase.verifyIntegrityOf loanApplication.getApplicationId()

        then: "Application should be accepted"
            loanApplication.status.value == StatusValue.APPLICATION_INTEGRITY_PASSED
    }

    def "Loan application should be rejected when invalid customer email provided" () {

        given: "Loan application"
            def loanApplication = com.poccofinance.lms.usecase.TestDataFactory.LoanApplicationFactory.validWithEmailLike email
            repository.findById(loanApplication.getApplicationId()) >> loanApplication

        when: "Application processing is executed"
            verifyLoanAplicationIntegrityUseCase.verifyIntegrityOf loanApplication.getApplicationId()

        then: "Application should be rejected"
            loanApplication.status.value == StatusValue.REJECTED

        where:
            email << [null, ""]
    }

}
