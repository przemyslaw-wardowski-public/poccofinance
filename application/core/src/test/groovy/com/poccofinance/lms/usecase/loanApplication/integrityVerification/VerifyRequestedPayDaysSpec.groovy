package com.poccofinance.lms.usecase.loanApplication.integrityVerification

import com.poccofinance.lms.entity.ProductTerms
import com.poccofinance.lms.entity.StatusValue
import com.poccofinance.lms.usecase.TestDataFactory
import spock.lang.Specification

import java.time.LocalTime

class VerifyRequestedPayDaysSpec extends Specification{

    LoanApplicationRepository repository = Mock(LoanApplicationRepository)
    VerifyLoanApplicationIntegrityUseCase reviewLoanAplicationUseCase = new VerifyLoanApplicationIntegrityUseCase(repository)

    def "Loan should be rejected when requested repay days are out of product terms" () {

        given: "Loan application for product"
            def terms = new ProductTerms(BigDecimal.ONE, BigDecimal.TEN, minAllowedRepayDays, maxAllowedRepayDays, LocalTime.MIN, LocalTime.MAX, 30)
            def product = TestDataFactory.ProductTestFactory.validWithTerms terms
            def loanApplication = TestDataFactory.LoanApplicationFactory.validWithProductAndRepayDaysLike product, requestedRepayDays
            repository.findById(loanApplication.getApplicationId()) >> loanApplication

        when: "Application for loan is processed "
            reviewLoanAplicationUseCase.verifyIntegrityOf loanApplication.getApplicationId()

        then: "Application should be rejected"
            loanApplication.status.value == StatusValue.REJECTED

        where:
            minAllowedRepayDays            | maxAllowedRepayDays      | requestedRepayDays
            5                              | 30                       | 2
            5                              | 30                       | 31
    }


}
