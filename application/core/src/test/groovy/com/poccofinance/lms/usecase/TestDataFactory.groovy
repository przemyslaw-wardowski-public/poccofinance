package com.poccofinance.lms.usecase

import com.poccofinance.lms.entity.LoanApplication
import com.poccofinance.lms.entity.LoanProduct
import com.poccofinance.lms.entity.ProductTerms
import com.poccofinance.lms.entity.RequestedLoanTerms

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

class TestDataFactory {

    static class RequestedLoanTermsFactory {
        static def valid() {
            return new RequestedLoanTerms(BigDecimal.TEN, 30)
        }
    }

    static class LoanApplicationFactory {

        static def valid() {
            ProductTerms terms = ProductTermsTestFactory.valid()
            RequestedLoanTerms requestedLoanTerms = new RequestedLoanTerms(BigDecimal.TEN, 10)
            LocalTime validTime = terms.getMinAllowedApplicationTime().plusMinutes(1)
            LocalDateTime validDateTime = LocalDateTime.of(LocalDate.now(), validTime)
            LoanProduct validPoduct = ProductTestFactory.valid()
            return new LoanApplication(UUID.randomUUID(), "customer@email.com", validDateTime, requestedLoanTerms, validPoduct)
        }

        static def validWithProductAndRequestedAmountLike (LoanProduct loanProduct, BigDecimal requestedAmount) {
            def app = valid()
            app.selectedProduct = loanProduct
            app.requestedLoanTerms.requestedAmount = requestedAmount
            return app
        }

        static def validWithRequestedMaxAmountRequestedAt (LoanProduct loanProduct, LocalDateTime appliedAt) {
            def app = valid()
            app.appliedAt = appliedAt
            app.selectedProduct = loanProduct
            app.requestedLoanTerms.requestedAmount = loanProduct.getTerms().getMaxAllowedBorrowAmount()
            return app
        }

        static def validWithProduct (LoanProduct loanProduct) {
            def app = valid()
            app.selectedProduct = loanProduct
            return app
        }


        static def validWithEmailLike (String email) {
            def app = valid()
            app.customerEmail = email
            return app
        }

        static def validWithProductAndRepayDaysLike (LoanProduct loanProduct, Integer repayDays) {
            def app = valid()
            app.requestedLoanTerms.requestedRepayDays = repayDays
            return app
        }
    }

    static class ProductTermsTestFactory {

        static def valid() {
            return new ProductTerms(BigDecimal.valueOf(5), BigDecimal.TEN, 5, 30, LocalTime.MIN, LocalTime.MAX, 30)
        }

        static def validWithMaxAllowedBorrowAmountLike (BigDecimal value){
            def terms = valid()
            terms.maxAllowedBorrowAmount = value
            return terms
        }

    }

    static class ProductTestFactory {

        static def valid() {
            def interestRate = BigDecimal.TEN
            def terms = ProductTermsTestFactory.valid()
            return new LoanProduct(interestRate, terms)
        }

        static def validWithTerms (ProductTerms terms) {
            def product = valid()
            product.terms = terms
            return product
        }

    }
}
