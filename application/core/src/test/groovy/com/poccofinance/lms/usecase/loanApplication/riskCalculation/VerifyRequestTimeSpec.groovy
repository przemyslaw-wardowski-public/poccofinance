package com.poccofinance.lms.usecase.loanApplication.riskCalculation

import com.poccofinance.lms.entity.ProductTerms
import com.poccofinance.lms.entity.StatusValue
import com.poccofinance.lms.usecase.TestDataFactory
import com.poccofinance.lms.usecase.loanApplication.integrityVerification.LoanApplicationRepository
import spock.lang.Specification

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

class VerifyRequestTimeSpec extends Specification{

    static LocalDateTime threeAtNight = LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT.plusHours(3))
    static LocalDateTime minuteAfterMidnight = LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT.plusMinutes(1))

    LoanApplicationRepository repository = Mock(LoanApplicationRepository)
    CalculateLoanRiskUseCase calculateLoanRiskUseCase = new CalculateLoanRiskUseCase(repository)

    def "Loan should be rejected when maximum amount is requested during specified time frame" () {

        given: "Loan application for product"
            def terms = new ProductTerms(BigDecimal.ONE, BigDecimal.TEN, 0, Integer.MAX_VALUE, LocalTime.of(6, 0), LocalTime.MAX, 30)
            def product = TestDataFactory.ProductTestFactory.validWithTerms terms
            def loanApplication = TestDataFactory.LoanApplicationFactory.validWithRequestedMaxAmountRequestedAt(product, requestedAt)
            repository.findById(loanApplication.getApplicationId()) >> loanApplication

        when: "Application for loan is processed "
            calculateLoanRiskUseCase.calculateRiskFor loanApplication.getApplicationId()

        then: "Application should be rejected"
            loanApplication.status.value == StatusValue.REJECTED

        where:
            requestedAt << [ threeAtNight, minuteAfterMidnight ]
    }


}
