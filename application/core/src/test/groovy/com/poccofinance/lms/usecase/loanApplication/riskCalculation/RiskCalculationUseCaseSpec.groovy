package com.poccofinance.lms.usecase.loanApplication.riskCalculation

import com.poccofinance.lms.entity.StatusValue
import com.poccofinance.lms.usecase.TestDataFactory
import com.poccofinance.lms.usecase.loanApplication.integrityVerification.LoanApplicationRepository
import spock.lang.Specification

class RiskCalculationUseCaseSpec extends Specification{


    LoanApplicationRepository repository = Mock(LoanApplicationRepository)
    CalculateLoanRiskUseCase calculateLoanRiskUseCase = new CalculateLoanRiskUseCase(repository)

    def "Loan application should be accepted when product terms are fulfilled" () {

        given: "Loan application with all data provided"
            def loanApplication = TestDataFactory.LoanApplicationFactory.valid()
            repository.findById(loanApplication.getApplicationId()) >> loanApplication

        when: "Application processing is executed"
        calculateLoanRiskUseCase.calculateRiskFor loanApplication.getApplicationId()

        then: "Application should be accepted"
            loanApplication.status.value == StatusValue.ACCEPTED
    }

}
