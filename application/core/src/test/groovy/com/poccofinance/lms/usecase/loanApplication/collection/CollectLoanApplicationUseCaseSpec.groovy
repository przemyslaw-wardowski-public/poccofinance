package com.poccofinance.lms.usecase.loanApplication.collection

import com.poccofinance.lms.usecase.TestDataFactory
import spock.lang.Specification

class CollectLoanApplicationUseCaseSpec extends Specification{

    def loanApplicationRepository = Stub(LoanApplicationCollectingRepository )
    def productPortfolioLookup = Stub(ProductPortfolioLookupRepository)
    def collectLoanApplicationUseCase = new CollectLoanApplicationUseCase( productPortfolioLookup, loanApplicationRepository)

    def "Loan application should not be collected when product does not exist" () {

        given: "Product lookup that will not find product"
            productPortfolioLookup.findOurBestOneAndOnlySuperLoanProductForChristmas() >> null

        and: "Loan application data"
            def applicationId = UUID.randomUUID()
            def customerEmail = "custom@email.com"
            def requestedTerms = TestDataFactory.RequestedLoanTermsFactory.valid()

        when: "Application collection takes place"
            collectLoanApplicationUseCase.collect(applicationId, customerEmail, requestedTerms)

        then: "Exception about missing product is thrown"
            def exception = thrown(AssertionError)
            exception.message == "Loan product not selected"
    }

    def "Loan application should be collected when product is available" () {

        given: "Product lookup that will not find product"
            productPortfolioLookup.findOurBestOneAndOnlySuperLoanProductForChristmas() >> TestDataFactory.ProductTestFactory.valid()

        and: "Loan application data"
            def applicationId = UUID.randomUUID()
            def customerEmail = null
            def requestedTerms = null

        when: "Application collection takes place"
            collectLoanApplicationUseCase.collect(applicationId, customerEmail, requestedTerms)

        then: "Exception about missing product is not thrown"
            notThrown(AssertionError)
    }


}
