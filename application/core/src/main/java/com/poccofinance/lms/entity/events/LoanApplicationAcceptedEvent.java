package com.poccofinance.lms.entity.events;

import java.time.LocalDateTime;
import java.util.UUID;

public class LoanApplicationAcceptedEvent extends DomainEvent{

    public LoanApplicationAcceptedEvent(UUID aggregateId, LocalDateTime createdAt) {
        super(aggregateId, createdAt);
    }

}
