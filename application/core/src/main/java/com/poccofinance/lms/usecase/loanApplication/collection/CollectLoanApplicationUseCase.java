package com.poccofinance.lms.usecase.loanApplication.collection;

import com.poccofinance.lms.entity.LoanApplication;
import com.poccofinance.lms.entity.LoanProduct;
import com.poccofinance.lms.entity.RequestedLoanTerms;

import java.time.LocalDateTime;
import java.util.UUID;

public class CollectLoanApplicationUseCase {

    private final ProductPortfolioLookupRepository productPortfolioLookup;
    private final LoanApplicationCollectingRepository loanApplicationRepository;

    public CollectLoanApplicationUseCase(ProductPortfolioLookupRepository productPortfolioLookup, LoanApplicationCollectingRepository loanApplicationRepository) {
        this.productPortfolioLookup = productPortfolioLookup;
        this.loanApplicationRepository = loanApplicationRepository;
    }

    public void collect(UUID applicationId, String customerEmail, RequestedLoanTerms requestedTerms) {
        LoanProduct product = productPortfolioLookup.findOurBestOneAndOnlySuperLoanProductForChristmas();
        LocalDateTime applicationTime = LocalDateTime.now();
        LoanApplication loanApplication = new LoanApplication(applicationId, customerEmail, applicationTime, requestedTerms, product);
        loanApplicationRepository.save(loanApplication);
    }

}
