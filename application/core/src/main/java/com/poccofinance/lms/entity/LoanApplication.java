package com.poccofinance.lms.entity;

import com.poccofinance.lms.entity.events.LoanApplicationAcceptedEvent;
import com.poccofinance.lms.entity.events.LoanApplicationCreatedEvent;
import com.poccofinance.lms.entity.events.LoanApplicationIntegrityPassedEvent;
import com.poccofinance.lms.entity.events.LoanApplicationRejectedEvent;

import java.time.LocalDateTime;
import java.util.UUID;

public class LoanApplication extends AggregateRoot{

    private UUID applicationId;
    private String customerEmail;
    private LocalDateTime appliedAt;
    private RequestedLoanTerms requestedLoanTerms;
    private LoanApplicationStatus status;
    private LoanProduct selectedProduct;

    public LoanApplication(){
    }

    public LoanApplication(UUID applicationId, String customerEmail, LocalDateTime appliedAt, RequestedLoanTerms requestedLoanTerms, LoanProduct selectedProduct ) {
        assert selectedProduct != null : "Loan product not selected";
        apply(new LoanApplicationCreatedEvent(applicationId, customerEmail, appliedAt, requestedLoanTerms, selectedProduct));
    }

    public void accept() {
        apply(new LoanApplicationAcceptedEvent(applicationId, LocalDateTime.now()));
    }

    public void reject(String reason) {
        apply(new LoanApplicationRejectedEvent(applicationId, LocalDateTime.now(), reason));
    }

    public void markIntegrityPassed() {
        apply(new LoanApplicationIntegrityPassedEvent(applicationId, LocalDateTime.now()));
    }

    public LoanApplication apply(LoanApplicationAcceptedEvent event) {
        this.status = new LoanApplicationStatus(StatusValue.ACCEPTED);
        rememberChange(event);
        return this;
    }

    public LoanApplication apply(LoanApplicationRejectedEvent event) {
        this.status = new LoanApplicationStatus(StatusValue.REJECTED, event.getReason());
        rememberChange(event);
        return this;
    }

    public LoanApplication apply(LoanApplicationIntegrityPassedEvent event) {
        this.status = new LoanApplicationStatus(StatusValue.APPLICATION_INTEGRITY_PASSED);
        rememberChange(event);
        return this;
    }

    public LoanApplication apply(LoanApplicationCreatedEvent event) {
        this.applicationId = event.getAggregateId();
        this.customerEmail = event.getCustomerEmail();
        this.appliedAt = event.getCreatedAt();
        this.requestedLoanTerms = event.getRequestedLoanTerms();
        this.status = new LoanApplicationStatus(StatusValue.CREATED);
        this.selectedProduct = event.getSelectedProduct();
        rememberChange(event);
        return this;
    }

    public LocalDateTime getAppliedAt() {
        return appliedAt;
    }

    public UUID getApplicationId() {
        return applicationId;
    }

    public RequestedLoanTerms getRequestedLoanTerms() {
        return requestedLoanTerms;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public LoanApplicationStatus getStatus() {
        return status;
    }

    public LoanProduct getSelectedProduct() {
        return selectedProduct;
    }
}
