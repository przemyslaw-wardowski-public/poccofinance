package com.poccofinance.lms.usecase.loan.postponeRepayDate;

import com.poccofinance.lms.entity.Loan;

import java.util.UUID;

public interface LoanRepository {
    void save(Loan loan);

    Loan findById(UUID loanId);
}
