package com.poccofinance.lms.entity;

import java.math.BigDecimal;

public class RequestedLoanTerms {

    private BigDecimal requestedAmount;
    private Integer requestedRepayDays;

    public RequestedLoanTerms(BigDecimal requestedAmount, Integer requestedRepayDays) {
        this.requestedAmount = requestedAmount;
        this.requestedRepayDays = requestedRepayDays;
    }

    public BigDecimal getRequestedAmount() {
        return requestedAmount;
    }

    public Integer getRequestedRepayDays() {
        return requestedRepayDays;
    }

}
