package com.poccofinance.lms.entity.events;

import java.time.LocalDateTime;
import java.util.UUID;

public class LoanApplicationIntegrityPassedEvent extends DomainEvent{

    public LoanApplicationIntegrityPassedEvent(UUID aggregateId, LocalDateTime createdAt) {
        super(aggregateId, createdAt);
    }

}
