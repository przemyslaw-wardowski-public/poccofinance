package com.poccofinance.lms.entity;

import java.math.BigDecimal;
import java.time.LocalTime;

public class ProductTerms {

    private BigDecimal minAllowedBorrowAmount;
    private BigDecimal maxAllowedBorrowAmount;
    private int minAllowedRepayDays;
    private int maxAllowedRepayDays;
    private LocalTime minAllowedApplicationTime;
    private LocalTime maxAllowedApplicationTime;
    private int postponeRepayDays;

    public ProductTerms(BigDecimal minAllowedBorrowAmount, BigDecimal maxAllowedBorrowAmount, int minAllowedRepayDays, int maxAllowedRepayDays, LocalTime minAllowedApplicationTime, LocalTime maxAllowedApplicationTime, int postponeRepayDays) {
        this.minAllowedBorrowAmount = minAllowedBorrowAmount;
        this.maxAllowedBorrowAmount = maxAllowedBorrowAmount;
        this.minAllowedRepayDays = minAllowedRepayDays;
        this.maxAllowedRepayDays = maxAllowedRepayDays;
        this.minAllowedApplicationTime = minAllowedApplicationTime;
        this.maxAllowedApplicationTime = maxAllowedApplicationTime;
        this.postponeRepayDays = postponeRepayDays;
    }

    public BigDecimal getMinAllowedBorrowAmount() {
        return minAllowedBorrowAmount;
    }

    public BigDecimal getMaxAllowedBorrowAmount() {
        return maxAllowedBorrowAmount;
    }

    public int getMinAllowedRepayDays() {
        return minAllowedRepayDays;
    }

    public int getMaxAllowedRepayDays() {
        return maxAllowedRepayDays;
    }

    public LocalTime getMinAllowedApplicationTime() {
        return minAllowedApplicationTime;
    }

    public LocalTime getMaxAllowedApplicationTime() {
        return maxAllowedApplicationTime;
    }

    public int getPostponeRepayDays() {
        return postponeRepayDays;
    }
}
