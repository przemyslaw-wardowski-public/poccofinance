package com.poccofinance.lms.entity.events;

import com.poccofinance.lms.entity.LoanApplication;

import java.time.LocalDateTime;
import java.util.UUID;

public class LoanGrantedEvent extends DomainEvent{

    private final LoanApplication loanApplication;

    public LoanGrantedEvent(UUID aggregateId, LocalDateTime createdAt, LoanApplication loanApplication) {
        super(aggregateId, createdAt);
        this.loanApplication = loanApplication;
    }

    public LoanApplication getLoanApplication() {
        return loanApplication;
    }
}
