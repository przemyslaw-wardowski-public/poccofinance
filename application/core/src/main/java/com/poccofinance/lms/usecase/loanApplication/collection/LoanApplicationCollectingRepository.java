package com.poccofinance.lms.usecase.loanApplication.collection;

import com.poccofinance.lms.entity.LoanApplication;

public interface LoanApplicationCollectingRepository {

    void save(LoanApplication loanApplication);

}
