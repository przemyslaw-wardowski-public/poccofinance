package com.poccofinance.lms.entity;

public enum StatusValue {
    CREATED,
    APPLICATION_INTEGRITY_PASSED,
    ACCEPTED,
    REJECTED
}