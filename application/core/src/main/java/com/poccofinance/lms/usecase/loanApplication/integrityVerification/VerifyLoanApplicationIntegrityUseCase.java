package com.poccofinance.lms.usecase.loanApplication.integrityVerification;

import com.poccofinance.lms.entity.LoanApplication;
import com.poccofinance.lms.entity.ProductTerms;
import com.poccofinance.lms.entity.RequestedLoanTerms;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

import static ir.cafebabe.math.utils.BigDecimalUtils.is;

public class VerifyLoanApplicationIntegrityUseCase {

    private LoanApplicationRepository loanApplicationRepository;

    public VerifyLoanApplicationIntegrityUseCase(LoanApplicationRepository loanApplicationRepository) {
        this.loanApplicationRepository = loanApplicationRepository;
    }

    public void verifyIntegrityOf(UUID loanApplicationId) {

        LoanApplication loanApplication = loanApplicationRepository.findById(loanApplicationId);

        Optional<String> validationResults = performProductTermsValidation(loanApplication);

        if(validationResults.isPresent()){
            loanApplication.reject(validationResults.get());
        }else{
            loanApplication.markIntegrityPassed();
        }

        loanApplicationRepository.save(loanApplication);
    }

    private Optional<String> performProductTermsValidation(LoanApplication loanApplication) {
        ProductTerms productTerms = loanApplication.getSelectedProduct().getTerms();
        RequestedLoanTerms requestedTerms = loanApplication.getRequestedLoanTerms();

        Stream<?> applicationMandatoryFields = Stream.of(
                loanApplication.getCustomerEmail(),
                requestedTerms.getRequestedAmount(),
                requestedTerms.getRequestedRepayDays());

        if (applicationMandatoryFields.anyMatch(Objects::isNull))
            return Optional.of("All application fields are mandatory");

        if (StringUtils.isBlank(loanApplication.getCustomerEmail()))
            return Optional.of("Incorrect customer email");

        if (is(loanApplication.getRequestedLoanTerms().getRequestedAmount()).gt(productTerms.getMaxAllowedBorrowAmount()))
            return Optional.of(String.format("Requested amount [%s] is bigger than max allowed [%s]", loanApplication.getRequestedLoanTerms().getRequestedAmount(), productTerms.getMaxAllowedBorrowAmount()));

        if(is(loanApplication.getRequestedLoanTerms().getRequestedAmount()).lt(productTerms.getMinAllowedBorrowAmount()))
            return Optional.of("Requested amount is smaller than min allowed");

        if(loanApplication.getRequestedLoanTerms().getRequestedRepayDays() > productTerms.getMaxAllowedRepayDays())
            return Optional.of("Requested repays days value is longer than max allowed");

        if(loanApplication.getRequestedLoanTerms().getRequestedRepayDays() < productTerms.getMinAllowedRepayDays())
            return Optional.of("Requested repays days value is shorter than min allowed");

        return Optional.empty();
    }

}