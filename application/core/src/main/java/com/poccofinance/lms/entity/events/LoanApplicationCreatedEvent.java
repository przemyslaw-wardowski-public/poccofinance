package com.poccofinance.lms.entity.events;

import com.poccofinance.lms.entity.LoanApplicationState;
import com.poccofinance.lms.entity.LoanProduct;
import com.poccofinance.lms.entity.RequestedLoanTerms;

import java.time.LocalDateTime;
import java.util.UUID;

public class LoanApplicationCreatedEvent extends DomainEvent{

    String customerEmail;
    RequestedLoanTerms requestedLoanTerms;
    LoanProduct selectedProduct;

    public LoanApplicationCreatedEvent(UUID applicationId, String customerEmail, LocalDateTime appliedAt, RequestedLoanTerms requestedLoanTerms, LoanProduct selectedProduct) {
        super(applicationId, appliedAt);
        this.customerEmail = customerEmail;
        this.requestedLoanTerms = requestedLoanTerms;
        this.selectedProduct = selectedProduct;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public RequestedLoanTerms getRequestedLoanTerms() {
        return requestedLoanTerms;
    }

    public LoanProduct getSelectedProduct() {
        return selectedProduct;
    }

}
