package com.poccofinance.lms.usecase.loanApplication.collection;

import com.poccofinance.lms.entity.LoanProduct;

public interface ProductPortfolioLookupRepository {

    LoanProduct findOurBestOneAndOnlySuperLoanProductForChristmas();

}
