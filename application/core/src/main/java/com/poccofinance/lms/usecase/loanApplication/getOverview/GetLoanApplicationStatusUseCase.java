package com.poccofinance.lms.usecase.loanApplication.getOverview;

import com.poccofinance.lms.entity.LoanApplication;
import com.poccofinance.lms.entity.LoanApplicationStatus;

import java.util.UUID;

public class GetLoanApplicationStatusUseCase {

    LoanApplicationRepository loanApplicationRepository;

    public GetLoanApplicationStatusUseCase(LoanApplicationRepository loanApplicationRepository) {
        this.loanApplicationRepository = loanApplicationRepository;
    }

    public LoanApplicationStatus getStatus(UUID loanApplicationId) {
        LoanApplication loanApplication = loanApplicationRepository.findById(loanApplicationId);
        return loanApplication.getStatus();
    }

}
