package com.poccofinance.lms.entity;

import java.util.Optional;

public class LoanApplicationStatus {

    private final StatusValue value;
    private final Optional<String> details;

    public LoanApplicationStatus(StatusValue currentApplicationStep, String details) {
        this.value = currentApplicationStep;
        this.details = Optional.ofNullable(details);
    }

    public LoanApplicationStatus(StatusValue applicationStep) {
        this(applicationStep, null);
    }

    public String getDetails() {
        return details.orElse("No additional details available");
    }

    public StatusValue getValue() {
        return value;
    }


}

