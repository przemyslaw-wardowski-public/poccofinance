package com.poccofinance.lms.usecase.loan.postponeRepayDate;

import com.poccofinance.lms.entity.Loan;

import java.util.UUID;

public class PostponeRepayDateUseCase {

    private LoanRepository loanRepository;

    public PostponeRepayDateUseCase(LoanRepository loanRepository) {
        this.loanRepository = loanRepository;
    }

    public void postponeRepayDate(UUID loanId) {
        Loan loan = loanRepository.findById(loanId);
        loan.postponeRepayDate();
        loanRepository.save(loan);
    }
}
