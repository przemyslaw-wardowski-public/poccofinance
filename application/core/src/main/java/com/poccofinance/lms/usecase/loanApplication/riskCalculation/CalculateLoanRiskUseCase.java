package com.poccofinance.lms.usecase.loanApplication.riskCalculation;

import com.poccofinance.lms.entity.LoanApplication;
import com.poccofinance.lms.entity.ProductTerms;
import com.poccofinance.lms.usecase.loanApplication.integrityVerification.LoanApplicationRepository;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.UUID;

import static ir.cafebabe.math.utils.BigDecimalUtils.is;

public class CalculateLoanRiskUseCase {

    private LoanApplicationRepository loanApplicationRepository;

    public CalculateLoanRiskUseCase(LoanApplicationRepository loanApplicationRepository) {
        this.loanApplicationRepository = loanApplicationRepository;
    }

    public void calculateRiskFor(UUID loanApplicationId){

        LoanApplication loanApplication = loanApplicationRepository.findById(loanApplicationId);

        if(isRisky(loanApplication)){
            loanApplication.reject("Risky one");
        }else{
            loanApplication.accept();
        }

        loanApplicationRepository.save(loanApplication);
    }

    private boolean isRisky(LoanApplication loanApplication) {
        ProductTerms productTerms = loanApplication.getSelectedProduct().getTerms();

        boolean appliedForMax = is(loanApplication.getRequestedLoanTerms().getRequestedAmount())
                .eq(loanApplication.getSelectedProduct().getTerms().getMaxAllowedBorrowAmount());

        LocalTime applicationTime = loanApplication.getAppliedAt().toLocalTime();

        boolean appliedAfterAllowed = applicationTime.isAfter(productTerms.getMaxAllowedApplicationTime());
        boolean appliedBeforeAllowd = applicationTime.isBefore(productTerms.getMinAllowedApplicationTime());
        boolean appliedTimeBeyondTerms = appliedAfterAllowed || appliedBeforeAllowd;

        return appliedForMax && appliedTimeBeyondTerms;
    }

}
