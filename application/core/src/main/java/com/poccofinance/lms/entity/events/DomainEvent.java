package com.poccofinance.lms.entity.events;

import com.poccofinance.lms.entity.LoanApplicationState;

import java.time.LocalDateTime;
import java.util.UUID;

public abstract class DomainEvent {

    private UUID aggregateId;
    private LocalDateTime createdAt;

    public DomainEvent(UUID aggregateId, LocalDateTime createdAt) {
        this.aggregateId = aggregateId;
        this.createdAt = createdAt;
    }

    public UUID getAggregateId() {
        return aggregateId;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

}
