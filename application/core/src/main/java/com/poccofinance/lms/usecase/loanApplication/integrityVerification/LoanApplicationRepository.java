package com.poccofinance.lms.usecase.loanApplication.integrityVerification;

import com.poccofinance.lms.entity.LoanApplication;

import java.util.UUID;

public interface LoanApplicationRepository {

    void save(LoanApplication loanApplication);

    LoanApplication findById(UUID loanApplicationId);
}
