package com.poccofinance.lms.usecase.loan.getOverview;

import com.poccofinance.lms.entity.Loan;

import java.util.UUID;

public interface LoanRepository {
    Loan findById(UUID loanId);
}
