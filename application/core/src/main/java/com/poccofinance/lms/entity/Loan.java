package com.poccofinance.lms.entity;

import com.poccofinance.lms.entity.events.LoanGrantedEvent;
import com.poccofinance.lms.entity.events.LoanRepayDatePostponedEvent;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

public class Loan extends AggregateRoot{

    private UUID loanID;
    private LocalDateTime createdAt;
    private LoanApplication loanApplication;
    private LocalDateTime repayDate;
    private BigDecimal interest;

    public Loan(){
        
    }

    public Loan(UUID loanID, LoanApplication loanApplication) {
        apply(new LoanGrantedEvent(loanID, LocalDateTime.now(), loanApplication));
    }

    private Loan apply(LoanGrantedEvent event) {
        this.loanID = event.getAggregateId();
        this.createdAt = event.getCreatedAt();
        this.loanApplication = event.getLoanApplication();
        this.repayDate = createdAt.plusDays(loanApplication.getRequestedLoanTerms().getRequestedRepayDays());
        this.interest = loanApplication.getRequestedLoanTerms().getRequestedAmount()
                .multiply(loanApplication.getSelectedProduct().getInterestRate())
                .divide(BigDecimal.valueOf(100));
        rememberChange(event);
        return this;
    }

    public void postponeRepayDate() {
        int extensionDays = loanApplication.getSelectedProduct().getTerms().getPostponeRepayDays();
        apply(new LoanRepayDatePostponedEvent(loanID, LocalDateTime.now(), extensionDays));
    }

    private Loan apply(LoanRepayDatePostponedEvent event) {
        this.repayDate = this.repayDate.plusDays(event.getExtensionDays());
        rememberChange(event);
        return this;
    }

    public BigDecimal getInterest() {
        return interest;
    }

    public LocalDateTime getRepayDate() {
        return repayDate;
    }

    public UUID getLoanID() {
        return loanID;
    }

    public BigDecimal getTotalAmounToPay(){
        return loanApplication.getRequestedLoanTerms().getRequestedAmount()
                .add(interest);
    }
}
