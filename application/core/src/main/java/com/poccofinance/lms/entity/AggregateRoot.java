package com.poccofinance.lms.entity;

import com.google.common.collect.ImmutableList;
import com.poccofinance.lms.entity.events.DomainEvent;

import java.util.ArrayList;
import java.util.List;

public class AggregateRoot {

    private List<DomainEvent> changes = new ArrayList<>();

    public List<DomainEvent> getChanges(){
        return ImmutableList.copyOf(changes);
    }

    public void flushChanges() {
        changes.clear();
    }

    public void rememberChange(DomainEvent domainEvent){
        changes.add(domainEvent);
    }

}
