package com.poccofinance.lms.entity;

import java.math.BigDecimal;

public class LoanProduct {

    private BigDecimal interestRate;
    private ProductTerms terms;

    public LoanProduct(BigDecimal interestRate, ProductTerms terms) {
        this.interestRate = interestRate;
        this.terms = terms;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }

    public ProductTerms getTerms() {
        return terms;
    }

}
