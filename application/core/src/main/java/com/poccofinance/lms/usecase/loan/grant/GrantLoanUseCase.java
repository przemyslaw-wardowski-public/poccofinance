package com.poccofinance.lms.usecase.loan.grant;

import com.poccofinance.lms.entity.Loan;
import com.poccofinance.lms.entity.LoanApplication;

import java.util.UUID;

public class GrantLoanUseCase {

    private LoanRepository loanRepository;
    private LoanApplicationRepository loanApplicationRepository;

    public GrantLoanUseCase(LoanApplicationRepository loanApplicationRepository, LoanRepository loanRepository) {
        this.loanRepository = loanRepository;
        this.loanApplicationRepository = loanApplicationRepository;
    }

    public void grantLoanFor(UUID loanApplicationId) {
        LoanApplication loanApplication = loanApplicationRepository.findById(loanApplicationId);
        Loan loan = new Loan(loanApplicationId, loanApplication);
        loanRepository.save(loan);
    }

}