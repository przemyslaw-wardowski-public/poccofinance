package com.poccofinance.lms.usecase.loan.grant;

import com.poccofinance.lms.entity.LoanApplication;

import java.util.UUID;

public interface LoanApplicationRepository {

    LoanApplication findById(UUID loanApplicationId);
}
