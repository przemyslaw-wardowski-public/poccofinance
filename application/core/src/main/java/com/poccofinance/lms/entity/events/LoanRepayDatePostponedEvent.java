package com.poccofinance.lms.entity.events;

import java.time.LocalDateTime;
import java.util.UUID;

public class LoanRepayDatePostponedEvent extends DomainEvent {

    private int extensionDays;

    public LoanRepayDatePostponedEvent(UUID loanID, LocalDateTime createdAt, int extensionDays) {
        super(loanID, createdAt);
        this.extensionDays = extensionDays;
    }

    public int getExtensionDays() {
        return extensionDays;
    }
}
