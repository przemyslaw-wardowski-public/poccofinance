package com.poccofinance.lms.usecase.loan.getOverview;

import com.poccofinance.lms.entity.Loan;

import java.util.UUID;

public class GetOverviewUseCase {

    private LoanRepository loanRepository;

    public GetOverviewUseCase(LoanRepository loanRepository) {
        this.loanRepository = loanRepository;
    }

    public Loan getDetailsOf(UUID loanId){
        return loanRepository.findById(loanId);
    }

}
