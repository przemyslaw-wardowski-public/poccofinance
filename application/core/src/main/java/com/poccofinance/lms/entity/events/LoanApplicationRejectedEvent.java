package com.poccofinance.lms.entity.events;

import java.time.LocalDateTime;
import java.util.UUID;

public class LoanApplicationRejectedEvent extends DomainEvent{

    private String reason;

    public LoanApplicationRejectedEvent(UUID aggregateId, LocalDateTime createdAt, String reason) {
        super(aggregateId, createdAt);
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }
}
