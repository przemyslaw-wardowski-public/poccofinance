package com.poccofinance.lms.usecase.loan.grant;

import com.poccofinance.lms.entity.Loan;

public interface LoanRepository {
    void save(Loan loan);
}
